"use strict";
import 'tablesort';
import 'flickity';
import 'choices.js';

(function($, Drupal) {
  Drupal.behaviors.mpiwgFS = {
    attach: function(context, settings) {
      if ($('.view-feature-stories').length > 0) {
        var title_h = 0;
        var text_h = 0;
        var author_h = 0;
        $('.view-feature-stories .views-row').each(function() {
          var that = this;
          if ($('h3', that).height() > title_h) {
            title_h = $('h3', that).height();
          }
          if ($('.field--name-field-fstory-teaser', that).height() > text_h) {
            text_h = $('.field--name-field-fstory-teaser', that).height();
          }
          if ($('.author', that).height() > author_h) {
            author_h = $('.author', that).height();
          }
        });
        $('.view-feature-stories .views-row h3').height(title_h);
        $('.view-feature-stories .views-row .field--name-field-fstory-teaser').height(text_h);
        $('.view-feature-stories .views-row .author').height(author_h);
      }
    }
  };
  Drupal.behaviors.mpiwgUmThCa = {
    attach: function(context, settings) {
      if ($('.block-views-blockprojects-umbrella-themes-grid-2').length > 0) {
        var title_h = 0;
        $('.block-views-blockprojects-umbrella-themes-grid-2 .views-row').each(function() {
          var that = this;
          if ($('h3', that).height() > title_h) {
            title_h = $('h3', that).height();
          }
        });
        $('.block-views-blockprojects-umbrella-themes-grid-2 .views-row h3').height(title_h);
      }
    }
  };

  Drupal.behaviors.alignImageCaptions = {
    attach: function(context, settings) {
      $('figure.align-center', context).each(function() {
        if ($(this).find('img').length > 0) {
          $(this).width($(this).find('img').width());
        }
      });
    }
  };

  Drupal.behaviors.mpiwg = {
    attach: function(context, settings) {

      $('.field--name-field-publications').once().each(function() {
        var that = this;
        $('.row', that).slice(0, 1).show();
        if ($('.row', that).length > 1) {
          var load_more = $('<a>', {
            'class': 'btn btn-primary',
            'text': Drupal.t('View more')
          });
          $(that).append(load_more);
          load_more.click(function() {
            $('.row:hidden', that).slice(0, 1).slideDown();
            if ($('.row:hidden').length == 0) {
              load_more.fadeOut('slow');
            }
            $('html,body').animate({
              scrollTop: load_more.offset().top
            }, 1500);
          });
        }
      });
    }
  };

  Drupal.behaviors.iconPosition = {
    attach: function (context, settings) {
      var image_wrapper = $('.lightbox_item .colorbox-image-inner', context);
      var icon = $('.iconfont_demo', context);

      image_wrapper.append(icon);
    }
  };
  
  
  Drupal.behaviors.searchPopup = {
    attach: function(context, settings) {
      console.log('HELLO');
      var trigger = $('#search-modal-trigger'),
        searchForm = $('#search-dropdown'),
        menuTrigger = $('.button--ultimenu'),
        menu = $('#ultimenu-main'),
        body = $('body'),
        searchField = $('#edit-search-api-fulltext');

      $(document).keydown(function(event) {
        if (event.which == 27) {
          searchForm.addClass('search-dropdown--hidden');
          $('.nav_filter-and-sort input.nav_accordion_check').removeAttr('checked');
        }
      });
      $(document).mouseup(function(event) {
        // if the target of the click isn't the trigger button, container nor a descendant of the container
        if (!trigger.is(event.target) && !searchForm.is(event.target) && searchForm.has(event.target).length === 0) {
          searchForm.addClass('search-dropdown--hidden').removeClass('search-dropdown--visible');
        }
      });

      trigger.once('search-popup').on('click', function(ev) {
        ev.preventDefault();
        searchForm.toggleClass('search-dropdown--hidden search-dropdown--visible');
        searchField.focus();

        if ($('[data-ultimenu-button]').is(":visible") && menu.is(":visible")) {
          menu.slideUp();
          body.removeClass('js-ultimenu-expanded');
          menuTrigger.removeClass('js-ultimenu-button-active');
        }
      });

      // Hide the search if the menu is opened.
      menuTrigger.once('menu-search-click').on('click', function(ev) {
        ev.preventDefault();

        if (!searchForm.hasClass('search-dropdown--hidden')) {
          searchForm.addClass('search-dropdown--hidden');
        }
      });
    }
  };

  Drupal.behaviors.mpwigSelect = {
    attach: function (context, settings) {
      var form = $(context).find('.views-exposed-form').once('form-select');

      if (form.length) {
        var elements = document.querySelectorAll('select.form-select');

        for (var i = 0; i < elements.length; i++) {
          var el = elements[i];
          new Choices(el);
        }
      }
    }
  };

  Drupal.behaviors.addToAny = {
    attach: function(context, settings) {
      var contentTitle = $('.block-entity-viewnode .h2', context),
        addToAnyBlock = $('.block-addtoany', context),
        button = addToAnyBlock.find('.button_share'),
        socialIcons = $('.addtoany_list');

      contentTitle.prepend(addToAnyBlock);

      button.once('addtoany').on('click', function(ev) {
        ev.preventDefault();
        socialIcons.toggleClass('active');
      });

      // events addtoany (not a block)
      $('.event-addtoany a.button_share', context).click(function(evt) {
        evt.preventDefault();
        $(this).next('.addtoany_list').toggleClass('active');
      });
    }
  };

  Drupal.behaviors.statusMessages = {
    attach: function(context, settings) {
      var messages = $('.messages', context),
        close = messages.find('.messages__close');

      close.on('click', function(ev) {
        ev.preventDefault();
        messages.fadeOut(300, function() {
          $(this).remove();
        });
      });
    }
  };

  Drupal.behaviors.linkTypes = {

      
    /**
     * Settings
     * */

      
    cssClasses: {    
      "type": {      
        "unknown": "linkTypeUnknown",
              "anker": "linkTypeAnker",
              "mail": "linkTypeMail",
              "download": "link-download",
              "page": "linkTypePage",
              "internal": "internal",
              "external": "external"    
      }  
    },

      attach: function(context) {    
      var elementsTest = "body .main-container .text-formatted a, body .main-container .field--type-text-long a";    
      var pointer = this;    
      jQuery(elementsTest, context).each(function(index) {      
        var link = new pointer.link(this);    
      });  
    },

      
    /**
     * The Link "Class"
     * */
      link: function(element) {    
      this.element = element;    
      var $element = jQuery(this.element);    
      this.url = jQuery(this.element).attr("href");    
      this.url = this.url == undefined ? "" : this.url;

           // check type
          
      if (typeof this.url !== 'string') {      
        this.type = "unknown";    
      } else if (this.url.indexOf("#") === 0) {      
        this.type = "anker";    
      } else if (this.url.indexOf("mailto:") !== -1) {      
        this.type = "mail";    
      } else if (this.url.indexOf("?token=") !== -1) {      
        this.type = "download";    
      } else {      
        var filePattern = '/\.7z$|\.aac$|\.arc$|\.arj$|\.asf$|\.asx$|\.avi$' +         '|\.csv$|\.doc?x$|\.exe$|\.flv$|\.gif$|\.gz$|\.gzip$|\.hqx$' +         '|\.jar$|\.jpe?g$|\.js$|\.mp(2|3|4|e?g)$|\.mov(ie)?$|\.msi$' +         '|\.msp$|\.pdf$|\.phps$|\.png$|\.ppt$|\.qtm?$' +         '|\.tar$|\.tgz$|\.torrent$|\.txt$|\.wav$|\.wma$|\.wmv$' +         '|\.wpd$|\.xls?x$|\.xml$|\.g?zip/';

              
        var fileExt = this.url.match(filePattern);      
        this.type = fileExt instanceof Array ? "download" : "page";    
      }

           // internal / external
          
      this.isInternal = ((this.url.indexOf("/") == 0 || this.url.indexOf(document.domain) >= 0)) ? 'internal' : 'external';

           // adding classes from settings
          
      $element.addClass(Drupal.behaviors.linkTypes.cssClasses.type[this.type]);    
      if (this.type !== "download") {      
        console.log(Drupal.behaviors.linkTypes.cssClasses.type[this.isInternal], this.element);      
        $element.addClass(Drupal.behaviors.linkTypes.cssClasses.type[this.isInternal]);    
      }  
    },
  };
})(jQuery, Drupal);