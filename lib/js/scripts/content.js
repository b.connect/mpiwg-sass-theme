(function ($) {
  Drupal.behaviors.toTop = {
    attach: function (context, settings) {
      $('a.to_top').once().on('click', function (e) {
        e.preventDefault();
        $("html, body").animate({
          scrollTop: 0
        }, 400);

        return false;
      });
    }
  }
})(jQuery);

