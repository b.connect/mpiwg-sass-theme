

$(document).ready(function() {
    $('form.nav_search input.search-input').keyup(function() {
        var lupe = $(this).parents('label').first().find('.icon-lupe');
        if($(this).val().length > 0) {
            if(!lupe.hasClass('width-toggled')) {
                lupe.animate({width:'toggle'},350).addClass('width-toggled');
            }
            $(this).addClass('search-input-wider');

        } else {
            lupe.animate({width:'toggle'},350).removeClass('width-toggled');
            $(this).removeClass('search-input-wider');
        }
    });
});
