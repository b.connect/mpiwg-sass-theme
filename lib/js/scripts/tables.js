var tableDescend = document.getElementById('sort-descend');
new Tablesort(tableDescend, { descending: true });
tableDescend.addEventListener('afterSort', function() {
    markSortedTableColumns();
});

$(document).ready(function() {
    prepareTablesForMobileView();
});

function markSortedTableColumns() {
    var columns = [];
    $(tableDescend).find('thead tr').first().find('th').each(function(index, element) {
        if(!!$(element).attr('aria-sort')) {
            columns.push(index);
        }
    });

    $(tableDescend).find('tbody tr').each(function() {
        $(this).find('th, td').each(function(index, element) {

            if(columns.indexOf(index) > -1) {
                $(element).addClass('column_sorted');
            } else {
                $(element).removeClass('column_sorted');
            }
        });
            
    });
}

/**
 * Adds Table column heading to each TD element in TBODY.
 * This can be used for a different views like display heading as label for each
 * TD on mobile view.
 */
function prepareTablesForMobileView() {
    $('table').each(function() {
        var tHeads = [];
        $(this).find('thead tr').first().find('th').each(function(index, element) {
            tHeads.push($(element).text());
        });
        $(this).find('tbody tr').each(function(index, element) {
            $(element).find('th, td').each(function(ind, el) {
                $(el).prepend('<span class="thead">' + tHeads[ind] + '</span>');
            });
        });
    });
}
